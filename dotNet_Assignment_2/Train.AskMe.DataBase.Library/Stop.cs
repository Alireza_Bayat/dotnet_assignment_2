namespace Train.AskMe.DataBase.Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Stop")]
    public partial class Stop
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Stop()
        {
            TrainTimes = new HashSet<TrainTime>();
        }

        [Key]
        public int StopNumber { get; set; }

        [Required]
        [Display(Name = "Stop Name")]
        [StringLength(100)]
        public string StopName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TrainTime> TrainTimes { get; set; }
    }
}

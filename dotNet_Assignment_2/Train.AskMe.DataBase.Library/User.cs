namespace Train.AskMe.DataBase.Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            CreatedRules = new HashSet<Rule>();
            ModifiedRules = new HashSet<Rule>();
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        public string AspNetUserId { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

        public virtual AspNetUser AspNetUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rule> CreatedRules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rule> ModifiedRules { get; set; }

        /// <summary>
        /// All rules created or modified
        /// </summary>
        /// <value>The rules.</value>
        public IEnumerable<Rule> Rules{
            get{
                return CombinedRules();
            }
        }

        /// <summary>
        /// Total number of approved rules
        /// </summary>
        public int ApprovedRulesCount
        {
            get
            {
                return GetRulesCount(QStatus.Approved);
            }
        }

        /// <summary>
        /// Total number of rejected rules
        /// </summary>
        public int RejectedRulesCount
        {
            get
            {
                return GetRulesCount(QStatus.Rejected);
            }
        }

        /// <summary>
        /// Total number of pending rules
        /// </summary>
        public int PendingRulesCount
        {
            get
            {
                return GetRulesCount(QStatus.Pending);
            }
        }

        /// <summary>
        /// Calculate the success rate by dividing the number of approved rules 
        /// by total of approved and rejected rules.
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:P2}")]
        public double SuccessRate
        {
            get
            {
                double success = 0;
                if(ApprovedRulesCount+RejectedRulesCount > 0)
                {
                    success = ApprovedRulesCount / (double)(ApprovedRulesCount + RejectedRulesCount);
                }
                return success;
            }
        }

        private IEnumerable<Rule> CombinedRules()
		{
            foreach (Rule rule in CreatedRules)
                yield return rule;

            foreach (Rule rule in ModifiedRules)
                yield return rule;
		}

        private int GetRulesCount(QStatus status)
        {
            int i = 0;
            foreach(Rule rule in Rules)
            {
                if (rule.Status == (int)status)
                    i++;
            }
            return i;
        }
    }
}

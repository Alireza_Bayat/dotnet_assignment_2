namespace Train.AskMe.DataBase.Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Rule")]
    public partial class Rule
    {
        public int RuleId { get; set; }

        [Required]
        [StringLength(150)]
        public string Question { get; set; }

        public string QuestionShort
        {
            get
            {
                return Question.Length > 50 ? string.Format("{0}...", Question.Substring(0, 50)) : Question;
            }
        }

        [Required]
        public int QuestionType { get; set; }

        [NotMapped]
        public QType QuestionTypeName
        {
            get
            {
                return (QType)QuestionType;
            }

            set
            {
                QuestionType = (int)value;
            }
        }

        [Required]
        [StringLength(200)]
        public string Answer { get; set; }

        public string AnswerShort
        {
            get
            {
                return Answer.Length > 50 ? string.Format("{0}...", Answer.Substring(0, 50)) : Answer;
            }
        }

        [Required]
        public int Status { get; set; }

        public QStatus QuestionStatusName
        {
            get
            {
                return (QStatus)Status;
            }
        }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public Guid CreatedById { get; set; }

        [Required]
        public DateTime LastModified { get; set; }

        [Required]
        public Guid LastModifiedById { get; set; }

        public virtual User CreatedBy { get; set; }

        public virtual User ModifiedBy { get; set; }
    }
}

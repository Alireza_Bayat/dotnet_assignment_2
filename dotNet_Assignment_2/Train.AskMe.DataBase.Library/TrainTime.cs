namespace Train.AskMe.DataBase.Library
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TrainTime")]
    public partial class TrainTime
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TimeId { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StopNumber { get; set; }

        [Key]
        [Column(Order = 1)]
        public TimeSpan Time { get; set; }

        public virtual Stop Stop { get; set; }
    }
}

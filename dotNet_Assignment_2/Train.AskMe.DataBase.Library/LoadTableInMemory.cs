﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Train.AskMe.DataBase.Library
{
    public class LoadTableInMemory : DataModel
    {
        public List<Rule> GetRules()
        {
            return Rules.ToList();
        }

        public List<Stop> GetStops()
        {
            return Stops.ToList();
        }
       
    }
}

﻿using System;
namespace Train.AskMe.DataBase.Library
{
    public enum QStatus
    {
        Rejected = -1,
        Pending = 0,
        Approved = 1
    }
}

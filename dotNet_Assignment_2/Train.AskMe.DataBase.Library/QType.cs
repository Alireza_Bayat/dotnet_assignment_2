﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Train.AskMe.DataBase.Library
{
    public enum QType
    {
        Simple = 0,

        [Display(Name = "Data Driven")]
        DataDriven = 1
    }
}

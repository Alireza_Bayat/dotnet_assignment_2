﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Train.AskMe.MVC;
using Train.AskMe.MVC.Controllers;


namespace Train.AskMe.MVC.Controllers
{
    [TestClass]
    public class RulesControllerTest
    {

        [TestMethod]
        public void Rules_Index()
        {
            RulesController controller = new RulesController();
            ActionResult result = controller.Index();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Rule_DetailsWithEmptyId_IsNotAView()
        {
            //You cannot view the details of a rule without knowing the rule id
            RulesController controller = new RulesController();
            Assert.IsNotInstanceOfType(controller.Details(null), typeof(ViewResult));
        }

        [TestMethod]
        public void Create()
        {
            RulesController controller = new RulesController();
            ActionResult result = controller.Create();

            Assert.IsNotNull(result);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Train.AskMe.BusinessLogic;
using Train.AskMe.BusinessLogic.Utilities;
using Train.AskMe.DataBase.Library;

namespace Train.AskMe.MVC.Tests
{
    [TestClass]
    public class BusinessLogicTest
    {
        [TestMethod]
        public void AvailablePlaceholders_Get_IsNotEmpty()
        {
            //Assert.IsTrue(Placeholder.AvailablePlaceholders.Count > 0);
        }

        [TestMethod]
        public void Is_Sanitizer_Removes_unnecessary_Characters()
        {
            string userInput = "))) )) ^^^ what %is CenTRal's Stop number please?";
            string sanitizedString = "what is central stop number";
            Assert.AreEqual(sanitizedString, string.Join(" ", Sanitizer.InputSanitizer(userInput)));
        }
        [TestMethod]
        public void GetKeywordExtractor_IsWorking()
        {
            string keyword = "";
            string correctKeyword = "the star";
            List<string> wordsInUserQuestion = new List<string> { "what", "is", "the", "star", "stop", "number" };
            string WordsInDbQuestion = "what is [@Station] stop number";
            string token = string.Empty;
            Extractors.KeywordExtractor(WordsInDbQuestion, wordsInUserQuestion, ref keyword, ref token);
            Assert.AreEqual(correctKeyword, keyword);
        }
        [TestMethod]
        public void Check_RuleType_And_Status_IsFixedRuleAndApproved()
        {

            string userInput = "How you can help me";
            userInput = string.Join(" ", Sanitizer.InputSanitizer(userInput));
            string answerPattern = "";
            string expectedPattern = "I can tell you station's number";
            Extractors.IsFixedRuleAndApproved(userInput, ref answerPattern);
            Assert.AreEqual(expectedPattern, answerPattern);
        }
        [TestMethod]
        public void Is_SetKeywordAndPattern_Returns_RightValues()
        {
            string userInput = "what is the star's stop number please?";
            string token;
            string result = Extractors.SetKeywordAndPattern(userInput, out token);
            Assert.AreEqual("[@station] stop number is [@stopnumber]", result);
        }

        [TestMethod]
        public void Users_ReturnEditors_EditorsOnly()
        {
            DataService db = new DataService();
            List<User> editors = db.GetAllEditors();
            List<User> nonEditors = new List<User>();
            foreach(User user in editors)
            {
                AspNetRole role = user.AspNetUser.AspNetRoles.Where(r => r.Id == DataService.EditorRoleId.ToString()).FirstOrDefault();
                if (role == null)
                    nonEditors.Add(user);
            }
            Assert.AreEqual(nonEditors.Count, 0);
        }

        [TestMethod]
        public void Users_ReturnEditorsWithSuccessRateNegative_ShouldBeZero()
        {
            DataService db = new DataService();
            List<User> editors = db.GetAllEditors();
            List<User> users = editors.Where(u => u.SuccessRate < 0).ToList();
            Assert.AreEqual(users.Count, 0);
        }

        [TestMethod]
        public void PlaceholderToken_NewNonExistingToken_InValid()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new PlaceholderToken("xyz"));
        }

        [TestMethod]
        public void PlaceholderToken_NewExistingToken_Valid()
        {
            Assert.IsInstanceOfType(new PlaceholderToken("[@station]"), typeof(PlaceholderToken));
        }

        [TestMethod]
        public void PlaceholderToken_StringWithNoToken_CountIsZero()
        {
            string test = "How can I help you";
            int expected = 0;
            Assert.AreEqual(expected, PlaceholderToken.ExtractPlaceholders(test).Count);
        }

        [TestMethod]
        public void PlaceholderToken_ExtractToken_CountTallies()
        {
            string test = "What [@station] is [@station]?";
            int expected = 2;
            Assert.AreEqual(expected, PlaceholderToken.ExtractPlaceholders(test).Count);
        }

        [TestMethod]
        public void PlaceholderToken_MatchPlaceholder_Found()
        {
            string test = "Hey [@station]";
            string expected = "[@station]";
            string actual = "";
            List<PlaceholderToken> list = PlaceholderToken.ExtractPlaceholders(test);
            actual = list[0].Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Rule_CheckIsRuleApproved_NonMatching()
        {
            string test = "x y z";
            string patt = string.Empty;
            Assert.IsFalse(Extractors.IsFixedRuleAndApproved(test, ref patt));
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Train.AskMe.BusinessLogic
{
    public static class Sanitizer
    {
        /// <summary>
        /// Remove anything from the string other than numbers and english alphabet characters.  
        /// Note: 's and "please" also will be removed
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static List<string> InputSanitizer(string input)
        {
            input = Regex.Replace(input, @"[^\w\s\']", string.Empty).ToLower().Replace("'s",string.Empty).Trim();
            List<string> cleanInput = new List<string>();
            cleanInput.AddRange(input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
            cleanInput.Remove("please");
            return cleanInput;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Train.AskMe.DataBase.Library;

namespace Train.AskMe.BusinessLogic
{
    public class DataService : IDisposable
    {

        private DataModel TrainAskMe;

        public static Guid ApproverRoleId = new Guid("B44E7967-BD36-49EE-9D0F-934E17EFFE06");
        public static Guid DatamaintainerRoleId = new Guid("DC6A2CFB-4384-48E4-A286-0D55BA0BDB22");
        public static Guid EditorRoleId = new Guid("316C1BEB-AF9A-4DD3-9114-FF918FB8A3AB");
        public static Dictionary<Guid, string> AvailableRoles = new Dictionary<Guid, string>()
        {
            {ApproverRoleId, "Approver" },
            {DatamaintainerRoleId, "Data-Maintainer" },
            {EditorRoleId, "Editor" }
        };

        private bool IsFirstRegistration
        {
            get
            {
                return TrainAskMe.AspNetUsers.Count() == 1;
            }
        }

        public DataService()
        {
            if (TrainAskMe == null)
                TrainAskMe = new DataModel();
        }

        public void Dispose(){
            if (TrainAskMe != null)
                TrainAskMe.Dispose();
        }

        #region Private Methods

        private void initialiseRoles()
        {
            if (TrainAskMe.AspNetRoles.Count() == 0)
            {
                foreach (KeyValuePair<Guid, string> role in AvailableRoles)
                    TrainAskMe.AspNetRoles.Add(new AspNetRole { Id = role.Key.ToString(), Name = role.Value });

                TrainAskMe.SaveChanges();
            }
        }

        private List<Rule> getAllRules()
        {
            return TrainAskMe.Rules.ToList();
        }

        #endregion Private

        public AspNetUser FindUser(string username)
        {
            return TrainAskMe.AspNetUsers.Where(u => u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
        }

        public User FindUserMetadata(Guid id)
        {
            return TrainAskMe.Users.Find(id);
        }

        public User FindUserMetadataByUserId(string aspNetUserId)
        {
            return TrainAskMe.Users.Where(u => u.AspNetUserId.Equals(aspNetUserId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
        }
        /// <summary>
        /// Update the first name and last name of the registered user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public bool UpdateUserMetadata(string username, string firstName, string lastName)
        {
            bool success = false;

            AspNetUser registeredUser = TrainAskMe.AspNetUsers
                                                  .Include("Users")
                                                  .Where(u => u.UserName.Equals(username)).FirstOrDefault();
            if (registeredUser != null && registeredUser.Users.Count() < 1)
            {
                if (IsFirstRegistration)
                {
                    initialiseRoles();

                    TrainAskMe.AspNetRoles.Add(
                        TrainAskMe.AspNetRoles
                        .Where(r => r.Id.Equals(DatamaintainerRoleId.ToString(), StringComparison.InvariantCultureIgnoreCase)).First());
                }

                TrainAskMe.Users.Add(new User
                {
                    Id = Guid.NewGuid(),
                    AspNetUserId = registeredUser.Id,
                    FirstName = firstName,
                    LastName = lastName
                });

                TrainAskMe.SaveChanges();

                success = true;
            }

            return success;
        }
        /// <summary>
        /// Update the role of the registered user
        /// </summary>
        /// <param name="aspNetUserId"></param>
        /// <param name="isEditor"></param>
        /// <param name="isApprover"></param>
        /// <param name="isDataMaintainer"></param>
        /// <returns></returns>
        public bool UpdateRoles(string aspNetUserId, bool isEditor, bool isApprover, bool isDataMaintainer)
        {
            bool success = false;
            AspNetUser asUser = TrainAskMe.AspNetUsers.Find(aspNetUserId);
            if (asUser != null)
            {
                asUser.AspNetRoles.Clear();
                if (isEditor)
                    asUser.AspNetRoles.Add(TrainAskMe.AspNetRoles.Find(EditorRoleId.ToString()));
                if (isApprover)
                    asUser.AspNetRoles.Add(TrainAskMe.AspNetRoles.Find(ApproverRoleId.ToString()));
                if (isDataMaintainer)
                    asUser.AspNetRoles.Add(TrainAskMe.AspNetRoles.Find(DatamaintainerRoleId.ToString()));

                TrainAskMe.SaveChanges();

                success = true;
            }

            return success;
        }

        //Update xisting rule
        public bool UpdateRule(int ruleId, int type, string question, string answer, string editorId)
        {
            bool success = false;

            Rule existingRule = TrainAskMe.Rules.Where(r => r.RuleId == ruleId).FirstOrDefault();
            User editor = FindUserMetadataByUserId(editorId);
            if (editor == null)
                throw new Exception("Only a registered user can edit a rule");

            if (existingRule != null)
            {
                existingRule.Question = question;
                existingRule.Answer = answer;
                existingRule.QuestionType = type;
                existingRule.Status = (int)QStatus.Pending;
                existingRule.LastModified = DateTime.Now;
                existingRule.LastModifiedById = editor.Id;
                TrainAskMe.SaveChanges(); //refresh dataset
                success = true;
            }

            return success;
        }

        //Delete a rule from the database
        public bool DeleteRule(int ruleId)
        {
            bool success = false;

            Rule existingRule = TrainAskMe.Rules.Find(ruleId);
            if (existingRule != null)
            {
                TrainAskMe.Rules.Remove(existingRule);
                TrainAskMe.SaveChanges();
                success = true;
            }

            return success;
        }

        //Delete a stop from the database
        public bool DeleteStop(int stopId)
        {
            bool success = false;
            Stop existingStop = TrainAskMe.Stops.Find(stopId);
            if (existingStop != null)
            {
                TrainAskMe.Stops.Remove(existingStop);
                TrainAskMe.SaveChanges();
            }

            return success;
        }

        //Get all roles assigned to a registered user
        public List<Guid> GetRoles(string aspNetUserId)
        {
            List<Guid> roles = new List<Guid>();

            AspNetUser user = TrainAskMe.AspNetUsers.Include("AspNetRoles").Where(au=>au.Id.Equals( aspNetUserId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (user != null)
            {
                foreach (AspNetRole role in user.AspNetRoles)
                    roles.Add(new Guid(role.Id));
            }

            return roles;
        }

        //Get first name of registered user
        public string GetFirstName(string aspnetUserId)
        {
            AspNetUser registeredUser = TrainAskMe.AspNetUsers
                                                  .Include("Users")
                                                  .Where(asp => asp.Id.Equals(aspnetUserId)).FirstOrDefault();
            if (registeredUser != null && registeredUser.Users.Count() == 1)
                return registeredUser.Users.First().FirstName;

            return null;
        }

        //Search for a stop given its id
        public Stop FindStop(int id)
        {
            return TrainAskMe.Stops.Find(id);
        }

        //Search for a rule given its id
        public Rule FindRuleById(int ruleId)
        {
            return TrainAskMe.Rules.Find(ruleId);
        }

        //Get all train stops
        public List<Stop> GetAllStops()
        {
            return TrainAskMe.Stops.OrderBy(s => s.StopNumber).ToList();
        }

        //Get a list of all registered users
        public List<User> GetAllUsers()
        {
            return TrainAskMe.Users
                .Include("AspNetUser")
                .Include("AspNetUser.AspNetRoles")
                .ToList();
        }

        // Return a list of all editors from the database
        public List<User> GetAllEditors()
        {
            List<User> editors = new List<User>();
            var editorRole = TrainAskMe.AspNetRoles.Find(EditorRoleId.ToString());
            foreach (AspNetUser aspU in editorRole.AspNetUsers)
                editors.AddRange(aspU.Users);

            return editors;
        }

       // Generate a list of all approved rules for a user where they have created or changed a rule.
       public List<Rule> GetApprovedRulesFor(string aspNetUserId)
        {
            return TrainAskMe.Rules
                      .Where(r => r.Status == (int)QStatus.Approved
                        && (r.CreatedBy.AspNetUserId.Equals(aspNetUserId, StringComparison.InvariantCultureIgnoreCase)
                       || r.ModifiedBy.AspNetUserId.Equals(aspNetUserId, StringComparison.InvariantCultureIgnoreCase))
                            ).ToList();
        }

       // Generate a list of all rejected rules for a user where they have created or changed a rule.
       public List<Rule> GetRejectedRulesFor(string aspNetUserId)
        {
            return TrainAskMe.Rules
                              .Where(r => r.Status == (int)QStatus.Rejected
                    && (r.CreatedBy.AspNetUserId.Equals(aspNetUserId, StringComparison.InvariantCultureIgnoreCase)
                       || r.ModifiedBy.AspNetUserId.Equals(aspNetUserId, StringComparison.InvariantCultureIgnoreCase))
                                    ).ToList();
        }

        // Generate a list of all approved rules in the database.
        public List<Rule> GetAllApprovedRules()
        {
            return TrainAskMe.Rules.Where(r => r.Status == (int)QStatus.Approved).ToList();
        }

        //Generate a list of all rejected rules in the database
        public List<Rule> GetAllRejectedRules()
        {
            return TrainAskMe.Rules.Where(r => r.Status == (int)QStatus.Rejected).ToList();
        }

        //Generate a list of all pending rules in the database
        public List<Rule> GetAllPendingRules()
        {
            return TrainAskMe.Rules.Where(r => r.Status == (int)QStatus.Pending).ToList();
        }

        //Create a new simple or data-driven rule
        public bool CreateRule(int type, string question, string answer, string aspNetUserId)
        {
            User user = FindUserMetadataByUserId(aspNetUserId);
            if (user != null)
            {
                Rule ruleExists = TrainAskMe.Rules.Where(r => r.Question.Equals(question, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (ruleExists != null)
                    throw new Exception("This is a duplicate rule.");
                DateTime rightNow = DateTime.Now;
                TrainAskMe.Rules.Add(new Rule
                {
                    Question = question,
                    QuestionType = type,
                    Answer = answer,
                    Status = 0,
                    Created = rightNow,
                    CreatedById = user.Id,
                    LastModified = rightNow,
                    LastModifiedById = user.Id
                });
                TrainAskMe.SaveChanges();

                return true;
            }
            else
                throw new Exception("Sorry user account could not be found, rule cannot be created.");
        }

        //Create a new train stop
        public bool CreateStop(string stopName)
        {
            TrainAskMe.Stops.Add(new Stop { StopName = stopName });
            TrainAskMe.SaveChanges();
            return true;
        }

        //Change stop name
        public bool UpdateStop(int stopNumber, string newStopName)
        {
            Stop existingStop = FindStop(stopNumber);
            if (existingStop != null)
            {
                existingStop.StopName = newStopName;
                TrainAskMe.SaveChanges();
                return true;
            }
            else
                throw new Exception("Sorry, we couldn't find this stop.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Train.AskMe.DataBase.Library;

namespace Train.AskMe.BusinessLogic.Utilities
{
    public class PlaceholderToken
    {
        public static readonly string StationKey = "station";
        public static readonly string StopNumberKey = "stopNumber";

        private static Dictionary<string, string> availablePlaceholders = new Dictionary<string, string>()
        {
            { StationKey, "[@station]" },
            { StopNumberKey, "[@stopnumber]" }
        };

        private string name;
        private int index;

        public PlaceholderToken(string token)
        {
            string tokenFound = availablePlaceholders.Values.Where(v => v.Equals(token.ToLower(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (!string.IsNullOrEmpty(tokenFound))
            {
                name = tokenFound;
                index = -1;
            }
            else
                throw new ArgumentOutOfRangeException(token, "Sorry, this is not a valid placeholder.");
        }

        public PlaceholderToken(string token, int indexOfTokenInStatement){
            string tokenFound = availablePlaceholders.Values.Where(v => v.Equals(token.ToLower(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            if (!string.IsNullOrEmpty(tokenFound))
			{
				name = tokenFound;
                index = indexOfTokenInStatement;
			}
			else
				throw new ArgumentOutOfRangeException(token, "Sorry, this is not a valid placeholder.");
		}


        public static Dictionary<string, string> AvailablePlaceholders
        {
            get
            {
                return availablePlaceholders;
            }

            set
            {
                availablePlaceholders = value;
            }
        }

        public static List<PlaceholderToken> ExtractPlaceholders(string text){
            
            List<PlaceholderToken> placeholdersFound = new List<PlaceholderToken>();

            string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int i = 0;
            foreach (string word in words){
                string tokenFound = availablePlaceholders.Values.Where(v => word.ToLower().Contains(v)).FirstOrDefault();
                if (!string.IsNullOrEmpty(tokenFound))
                    placeholdersFound.Add(new PlaceholderToken(tokenFound, i));
                i++;
            }
            return placeholdersFound;
        }

        public static bool ValidateInputs(Rule rule, bool isQuestion)
        {
            string contents = isQuestion ? rule.Question : rule.Answer;

            if (!string.IsNullOrEmpty(contents))
            {
                if (!contents.Contains("[@"))
                    return false;
                if (!contents.Contains("]"))
                    return false;
                if (!PlaceholderToken.AvailablePlaceholders.ContainsValue(extractToken(contents)))
                    return false;
            }
            else
                return false;

            return true;
        }

        public static bool TokenRequirementIsValid(Rule rule)
        {
            if (rule.QuestionTypeName == QType.Simple)
                return true;
            if (ValidateInputs(rule, true))
            {
                string[] separatedWords = rule.Question.Split(new string[] { "[@" }, StringSplitOptions.RemoveEmptyEntries);
                return separatedWords.Count() == 2;
            }

            return false;
        }

        public static string extractToken(string text)
        {
            string valToReturn = text.Substring(text.IndexOf("[@"));
            valToReturn = valToReturn.Substring(0, valToReturn.IndexOf("]") + 1);

            return valToReturn;
        }

        public string Name { get { return name; } }
        public int Index { get { return index; } set { index = value; } }
    }
}

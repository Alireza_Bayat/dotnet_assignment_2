﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using Train.AskMe.BusinessLogic.Utilities;
using Train.AskMe.DataBase.Library;
using TrainRule = Train.AskMe.DataBase.Library.Rule;


namespace Train.AskMe.BusinessLogic
{
    public static class Extractors
    {
        static Dictionary<string, string> KeywordAndPattern = new Dictionary<string, string>();
        static string keyword = "";
        static string answerPattern = "";
        private static LoadTableInMemory dataModel = new LoadTableInMemory();

        /// <summary>
        /// Extract keyword from user's input and corresponding answer-pattern from Rule table 
        /// </summary>
        /// <param name="userQuestion"></param>
        /// <returns></returns>
        public static string SetKeywordAndPattern(string userQuestion, out string tokenFound)
        {
            string token = "";
            List<string> wordsInUserQuestion = Sanitizer.InputSanitizer(userQuestion);
            userQuestion = string.Join(" ", wordsInUserQuestion).ToLower();

            var drivenRules = dataModel.GetRules().Where(x => x.QuestionType == (int)QType.DataDriven).ToList();
            foreach (var item in drivenRules)
            {
                KeywordExtractor(item.Question, wordsInUserQuestion, ref keyword, ref token);
                string sanitisedDbQuestion = string.Join(" ", Sanitizer.InputSanitizer(item.Question.Replace(token, "")));
                string userQuestionKeywordRemoved = string.Join(" ", Sanitizer.InputSanitizer(userQuestion.Replace(keyword, ""))).ToLower();
                if (keyword != "" && userQuestionKeywordRemoved.Trim().SequenceEqual(sanitisedDbQuestion.Trim()))
                {
                    answerPattern = item.Answer;
                    break;
                }
            }
            List<PlaceholderToken> answerTokensFound = PlaceholderToken.ExtractPlaceholders(answerPattern);
            tokenFound = answerTokensFound.Count == 1 ? answerTokensFound[0].Name : string.Empty;
            tokenFound = "";
            return answerPattern;
        }
        /// <summary>
        /// Extract keywords from user's question
        /// </summary>
        /// <param name="dbQuestion"></param>
        /// <param name="wordInUserQuestion"></param>
        /// <param name="keyword"></param>
        public static void KeywordExtractor (string dbQuestion, List<string> wordInUserQuestion, ref string keyword, ref string questionToken)
        {
            List<string> wordInDbQuestion = Sanitizer.InputSanitizer(dbQuestion.Trim());
            List<PlaceholderToken> tokensFound = PlaceholderToken.ExtractPlaceholders(dbQuestion);

            if (tokensFound.Count == 1)    //We are accepting only one token for now.
            {
                PlaceholderToken token = tokensFound[0];
                questionToken = token.Name;
                if (wordInUserQuestion.Count() <= 2)
                {
                    keyword = string.Join(" ", wordInUserQuestion);
                    if ((int.TryParse(keyword, out int i))) { answerPattern = "[@station]"; return; }
                }
                else if (wordInDbQuestion.Count() == wordInUserQuestion.Count())
                {
                    keyword = wordInUserQuestion.ElementAt(token.Index).ToLower();
                    
                }
                else if (wordInUserQuestion.Count > wordInDbQuestion.Count)
                {
                    StringBuilder sb = new StringBuilder();
                    int difference = wordInUserQuestion.Count - wordInDbQuestion.Count;
                    for (int j = 0; j <= wordInDbQuestion.Count - difference; j++)
                    {
                        if (wordInUserQuestion.ElementAt(j) != wordInDbQuestion.ElementAt(j))
                        {
                            sb.Append(wordInUserQuestion.ElementAt(j));
                            for (int x = 0; x < difference; x++)
                            {
                                sb.Append(" ").Append(wordInUserQuestion.ElementAt(j+1));
                                j++;
                            }
                            keyword = sb.ToString().ToLower();
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Check wheather user's question is of a type of Fixed questiona and it's Approved
        /// </summary>
        /// <param name="userInput"></param>
        /// <param name="answerPattern"></param>
        /// <returns></returns>
        public static bool IsFixedRuleAndApproved(string userInput , ref string answerPattern)
        {
            try
            {
                var userQuestion = string.Join(" ", Sanitizer.InputSanitizer(userInput));
                var rule = dataModel.GetRules().Where
               (db => db.Question.Trim().ToLower() == userQuestion
               && db.Status == (int)QStatus.Approved 
               && db.QuestionType == (int)QType.Simple).FirstOrDefault();
                if (rule != null)
                    answerPattern = rule.Answer;

                return rule != null;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// Construct the answer based on user's question and corresponding answer-pattern from Rule table
        /// </summary>
        /// <param name="userQuestion"></param>
        /// <returns></returns>
        public static string GetAnswer(string userQuestion)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            string tokenFound = "";
            try
            {
                if (IsFixedRuleAndApproved(userQuestion, ref answerPattern))
                {
                    return answerPattern;
                }
                else
                {
                    SetKeywordAndPattern(userQuestion, out tokenFound);
                    List<Stop> stops = dataModel.GetStops().ToList();
                    if (int.TryParse(keyword, out int i))
                    {
                        var station = (from set in stops
                                       where (set.StopNumber == i)
                                       select ($"{set.StopName}")).First();
                        answerPattern = answerPattern.Replace(PlaceholderToken.AvailablePlaceholders[PlaceholderToken.StationKey], station.ToLower());
                        answerPattern = answerPattern.Replace(PlaceholderToken.AvailablePlaceholders[PlaceholderToken.StopNumberKey], keyword);
                    }
                    else
                    {
                        var stop = (from set in stops
                                    where (set.StopName.Trim().ToLower() == (keyword.Trim().ToLower()))
                                    select ($"{set.StopNumber}")).First();
                        if (answerPattern == "[@station]") { answerPattern = stop; }
                        else
                        {
                            answerPattern = answerPattern.Replace(PlaceholderToken.AvailablePlaceholders[PlaceholderToken.StopNumberKey], stop);
                            answerPattern = answerPattern.Replace(PlaceholderToken.AvailablePlaceholders[PlaceholderToken.StationKey], keyword);
                        }
                    }
                    return textInfo.ToTitleCase(answerPattern);
                }
            }
            catch (Exception e)
            {
                if (CultureInfo.CurrentCulture.Name.StartsWith("fr"))
                {
                    return ErrorMessageResource.NoAnswerFoundFR;
                }
                else
                {
                    return ErrorMessageResource.NoAnswerFoundEN;
                }
            }
            
        }

        
    }
}

﻿﻿window.onload = setup;

const SELECTABLE_ROW = "selectableRow";
const UNSELECTABLE_ROW = "unSelectableRow";
const HIGHLIGHT_ROW = "highlightRow";
const FORM_ELEMENT_ID = "selectedVar";


selected = null;


function setup() {
    var tags = document.getElementsByClassName(SELECTABLE_ROW);

    for (var i = 0; i < tags.length; i++) {
        tags[i].onclick = highlight;
    }

}


function highlight(event) {
    var el = document.getElementById(event.currentTarget.id);
    var formEl = document.getElementById(FORM_ELEMENT_ID);

    if (selected == null) {
        el.className = HIGHLIGHT_ROW;
        selected = el;
    }
    else if (selected != el) {
        selected.className = SELECTABLE_ROW;
        el.className = HIGHLIGHT_ROW;
        selected = el;
    }
    else if (selected == el) {
        selected.className = SELECTABLE_ROW;
        selected = null;
    }

    if (selected != null)
        formEl.value = event.currentTarget.id;
    else
        formEl.value = null;


}

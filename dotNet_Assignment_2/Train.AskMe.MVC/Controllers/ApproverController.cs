﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Train.AskMe.DataBase.Library;
using TrainRule = Train.AskMe.DataBase.Library.Rule;
using Train.AskMe.BusinessLogic;

namespace Train.AskMe.MVC.Controllers
{
    public enum Decision { APPROVE, REJECT };


    public class ApproverController : Controller
    {
        private DataModel db = new DataModel();

        // GET: Ref
        public ActionResult Index()
        {
            var set = from r in db.Rules
                      where r.Status == (int)QStatus.Pending
                      select r;

            return View(set.ToList());


        }



        // POST: Ref/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Decide(Decision button, string selected)
        {
            if (string.IsNullOrEmpty(selected) == true)
                return RedirectToAction("Index");

            int index = selected.IndexOf("_");
            selected = selected.Substring(index + 1);

            Rule r = db.Rules.Find(Convert.ToInt16(selected));

            if (r != null)
            {
                if (button == Decision.APPROVE)
                    r.Status = (int)QStatus.Approved;
                else if (button == Decision.REJECT)
                    r.Status = (int)QStatus.Rejected;
                else

                db.Entry(r).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");

        }

    }
}
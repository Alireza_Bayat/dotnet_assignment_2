﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Train.AskMe.BusinessLogic;
using Train.AskMe.DataBase.Library;
using Train.AskMe.MVC.Models;


namespace Train.AskMe.MVC.Controllers
{
    public class ReportsController : Controller
    {
        private DataService db = new DataService();

        // GET: Editors Approved Rules Report
        public ActionResult Index()
        {
            RulesReportViewModel reportdetails = new RulesReportViewModel();
            reportdetails.ApprovedRules = db.GetApprovedRulesFor(User.Identity.GetUserId());
            reportdetails.RejectedRules = db.GetRejectedRulesFor(User.Identity.GetUserId());
            return View(reportdetails);

        }

        // GET: All Editors Statistics Reports
        public ActionResult EditorsStats()
        {
            EditorsReportViewModel reportdetails = new EditorsReportViewModel();
            reportdetails.Editors = db.GetAllEditors();
            return View(reportdetails);

        }

        // GET: All Approved Rules Report
        public ActionResult Approved()
        {
            RulesReportViewModel reportdetails = new RulesReportViewModel();
            reportdetails.ApprovedRules = db.GetAllApprovedRules();
            reportdetails.RejectedRules = db.GetAllRejectedRules();
            return View(reportdetails);
        }

        public ActionResult Rejected()
        {
            RulesReportViewModel reportdetails = new RulesReportViewModel();
            reportdetails.ApprovedRules = db.GetAllApprovedRules();
            reportdetails.RejectedRules = db.GetAllRejectedRules();
            return View(reportdetails);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Train.AskMe.DataBase.Library;
using TrainRule = Train.AskMe.DataBase.Library.Rule;
using Train.AskMe.BusinessLogic;


namespace Train.AskMe.MVC.Controllers
{
    public enum ListMode { PENDING, REJECTED };
    public enum ModifyMode { Edit, Delete };


    public class EditorController : Controller
    {
        private DataModel db = new DataModel();


        // GET: Edit
        public ActionResult Index(ListMode button = ListMode.PENDING)
        {
            if (button == ListMode.PENDING)
            {
                ViewBag.mode = ListMode.PENDING;

                var set = from r in db.Rules
                          where r.Status == (int)QStatus.Pending
                          select r;

                return View(set.ToList());
            }
            else
            {
                ViewBag.mode = ListMode.REJECTED;

                var set = from r in db.Rules
                          where r.Status == (int)QStatus.Rejected
                          select r;

                return View(set.ToList());
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Train.AskMe.BusinessLogic;
using Train.AskMe.MVC.Models;

namespace Train.AskMe.MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ActionName("Index")]
        [ValidateAntiForgeryToken]
        public ActionResult AskQuestion(string question)
        {
            SubmittedQuestionViewModel searchResult = new SubmittedQuestionViewModel { Question = question, Answer = Extractors.GetAnswer(question) };
            ModelState.Clear();
            return View(searchResult);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Train.AskMe.BusinessLogic;
using Train.AskMe.BusinessLogic.Utilities;
using Train.AskMe.DataBase.Library;
using TrainRule = Train.AskMe.DataBase.Library.Rule;


namespace Train.AskMe.MVC.Controllers
{
    public class RulesController : Controller
    {
        private DataService db = new DataService();

        // GET: All pending rules by default
        public ActionResult Index()
        {
            var rules = db.GetAllPendingRules();
            return View(rules.ToList());
        }

        // GET: Rules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainRule rule = db.FindRuleById(id.Value);
            if (rule == null)
            {
                return HttpNotFound();
            }
            return View(rule);
        }

        // GET: Rules/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Question,QuestionTypeName,Answer")] TrainRule rule)
        {
            if (ModelState.IsValid)
            {
                if (!PlaceholderToken.TokenRequirementIsValid(rule))
                {
                    ModelState.AddModelError("Question", "Invalid token, please ensure you use a valid token and only one token in the question.");
                    return View(rule);
                }

                db.CreateRule((int)rule.QuestionTypeName, rule.Question, rule.Answer, User.Identity.GetUserId());
                return RedirectToAction("Index");
            }

            return View(rule);
        }

        // GET: Rules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainRule rule = db.FindRuleById(id.Value);
            if (rule == null)
            {
                return HttpNotFound();
            }
            return View(rule);
        }

        // POST: Rules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RuleId,Question,QuestionTypeName,Answer")] TrainRule rule)
        {
            if (ModelState.IsValid)
            {
                if (!PlaceholderToken.TokenRequirementIsValid(rule))
                {
                    ModelState.AddModelError("Question", "Invalid token, please ensure you use a valid token and only one token in the question.");
                    return View(rule);
                }

                db.UpdateRule(rule.RuleId, (int)rule.QuestionTypeName, rule.Question, rule.Answer, User.Identity.GetUserId());
                return RedirectToAction("Index");
            }
            return View(rule);
        }

        // GET: Rules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainRule rule = db.FindRuleById(id.Value);
            if (rule == null)
            {
                return HttpNotFound();
            }
            return View(rule);
        }

        // POST: Rules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.DeleteRule(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Train.AskMe.DataBase.Library;
using TrainRule = Train.AskMe.DataBase.Library.Rule;
using Train.AskMe.BusinessLogic;


namespace Train.AskMe.MVC.Controllers
{
    public enum ListMode { PENDING, REJECTED };

    public class EditController : Controller
    {
        private DataModel db = new DataModel();


        // GET: Edit
        public ActionResult Index(ListMode button = ListMode.PENDING)
        {
            DataService service = new DataService();

            if (button == ListMode.PENDING)
            {
                ViewBag.mode = ListMode.PENDING;

                var set = from r in db.Rules
                          where r.Status == 0
                          select r;

                return View(set.ToList());
            }
            else
            {
                ViewBag.mode = ListMode.REJECTED;

                var set = from r in db.Rules
                          where r.Status == -1
                          select r;

                return View(set.ToList());
            }
        }


        [HttpPost]
        public ActionResult Delete(string selected)
        {
            if (string.IsNullOrEmpty(selected) == true)
                return RedirectToAction("Index");

            int index = selected.IndexOf("_");
            selected = selected.Substring(index + 1);

            Rule r = db.Rules.Find(Convert.ToInt16(selected));
            db.Rules.Remove(r);
            db.SaveChanges();
            return RedirectToAction("Index");

        }


        /*[HttpPost]
        public ActionResult DecideOnRule(string selected, int status)
        {
            if (string.IsNullOrEmpty(selected) == true)
                return RedirectToAction("Index");

            int index = selected.IndexOf("_");
            selected = selected.Substring(index + 1);

            Rule r = db.Rules.Find(Convert.ToInt16(selected));
            r.Status = (int)status;
            db.Entry(r).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");

        }*/
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Train.AskMe.MVC.Startup))]
namespace Train.AskMe.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

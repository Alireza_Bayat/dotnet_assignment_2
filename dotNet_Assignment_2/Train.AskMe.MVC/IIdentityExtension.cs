﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Train.AskMe.BusinessLogic;
using Train.AskMe.DataBase.Library;

namespace Train.AskMe.MVC
{
    public static class IIdentityExtension
    {
        private static List<Guid> myRoleIds;

        //This method allows you to display the first name of the logged in user.
        public static string GetFirstName(this IIdentity userIdentity)
        {
            using(DataService db=new DataService())
            {
                return db.GetFirstName(userIdentity.GetUserId());
            }
        }

        //Check if logged in user has the Approver role.
        public static bool IsApprover(this IIdentity userIdentity)
        {
            return GetMyRoles(userIdentity).Contains(DataService.ApproverRoleId);
        }

        //Check if logged in user has the Editor role.
        public static bool IsEditor(this IIdentity userIdentity)
        {
            return GetMyRoles(userIdentity).Contains(DataService.EditorRoleId);
        }

        //Check if logged in user has the Data Maintainer role.
        public static bool IsDataMaintainer(this IIdentity userIdentity)
        {
            return GetMyRoles(userIdentity).Contains(DataService.DatamaintainerRoleId);
        }

        private static List<Guid> GetMyRoles(this IIdentity userIdentity)
        {
                using(DataService db=new DataService())
                {
                    myRoleIds = db.GetRoles(userIdentity.GetUserId());
                }

            return myRoleIds;
        }
    }
}
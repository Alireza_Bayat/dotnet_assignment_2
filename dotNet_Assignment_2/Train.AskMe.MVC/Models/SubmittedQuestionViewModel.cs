﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace Train.AskMe.MVC.Models
{
    public class SubmittedQuestionViewModel
    {
      
        public string Question { get; set; }

        public string Answer { get; set; }
    }
}
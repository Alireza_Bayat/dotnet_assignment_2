﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Train.AskMe.DataBase.Library;

namespace Train.AskMe.MVC.Models
{
    public class EditorsReportViewModel
    {
        public List<User> Editors { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double Average
        {
            get
            {
                double average = 0;
                int approvedSum = 0;
                int editorsCount = 0;

                foreach (User user in Editors)
                {
                    approvedSum += user.ApprovedRulesCount;
                    editorsCount++;
                }

                if (editorsCount > 0)
                {
                    average = approvedSum / (double)(editorsCount);
                }
                return average;
            }
        }
    }
}
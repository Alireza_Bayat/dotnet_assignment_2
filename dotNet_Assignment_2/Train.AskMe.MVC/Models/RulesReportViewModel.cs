﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Train.AskMe.DataBase.Library;

namespace Train.AskMe.MVC.Models
{
    public class RulesReportViewModel
    {
        public List<Rule> ApprovedRules { get; set; }
        public List<Rule> RejectedRules { get; set; }

        [DisplayFormat(DataFormatString = "{0:P2}")]
        public double SuccessRate
        {
            get
            {
                double average = 0;

                if (ApprovedRules.Count() + RejectedRules.Count() > 0)
                {
                    average = ApprovedRules.Count() / (double)(ApprovedRules.Count() + RejectedRules.Count());
                }
                return average;
            }
        }

        public int ApprovedRulesCount
        {
            get
            {
                return ApprovedRules.Count();
            }
        }

        public int RejectedRulesCount
        {
            get
            {
                return RejectedRules.Count();
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Train.AskMe.MVC.Models
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string AspNetUserId { get; set; }
        public bool Editor { get; set; }
        public bool Approver { get; set; }
        public bool DataMaintainer { get; set; }
    }
}